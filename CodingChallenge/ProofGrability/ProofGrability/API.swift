//
//  API.swift
//  ProofGrability
//
//  Description: this class connect the app with the api of the movie db
//
//  Created by Devstn4 on 3/20/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import Foundation
import Alamofire


enum Router : URLRequestConvertible {
    static var baseURLString = "https://api.themoviedb.org/3/"
    
    static var params = [Any]()
    
    // Movies
    case moviesPopular([String : AnyObject])
    
    case moviesTopRated([String : AnyObject])
    
    case moviesUpcoming([String : AnyObject])
    
    case movieSearch([String : AnyObject])
    
    case movie(String)
    
    // Genre Movies
    case genreMovies([String : AnyObject])
    
    // Series
    case seriesPopular([String : AnyObject])
    
    case seriesTopRated([String : AnyObject])
    
    case seriesOnTV([String : AnyObject])
    
    case serie(String)
    
    var method: HTTPMethod {
        switch self {
            
            // Movies
            case .moviesPopular:
                return .get
            
            case .moviesTopRated:
                return .get
            
            case .moviesUpcoming:
                return .get
            
            case .movieSearch:
                return .get
            
            case .movie:
                return .get
            
            // Genres Movies
            case .genreMovies:
                return .get
            
            // Series
            case .seriesPopular:
                return .get
            
            case .seriesTopRated:
                return .get
            
            case .seriesOnTV:
                return .get
            
            case .serie:
                return .get
        }
    }
    
    var path: String {
        switch  self {
            
            // Movies
            case .moviesPopular:
                return "movie/popular"
            
            case .moviesTopRated:
                return "movie/top_rated"
            
            case .moviesUpcoming:
                return "movie/upcoming"
            
            case .movieSearch:
                return "search/movie"
            
            case .movie(let param):
                return "movie/\(param)"
            
            // Genres
            case .genreMovies:
                return "genre/movie/list"
            
            // Series
            case .seriesPopular:
                return "tv/popular"
            
            case .seriesTopRated:
                return "tv/top_rated"
            
            case .seriesOnTV:
                return "tv/on_the_air"
            
            case .serie(let param):
                return "tv/\(param)"
            
            default:
                return ""
            
        }
    }
    
    /*
     *  description: function that realize the encoding of the url
     */
    func asURLRequest() throws -> URLRequest {
        
        let URL = try Router.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: URL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch  self {

            // Movies
            case .moviesPopular(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .moviesTopRated(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .moviesUpcoming(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .movieSearch(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .movie:
                let param = ["api_key": "6e91acae467fcd5fad05efc3e5dae6fd", "language": "en-US",] as [String : Any]
                return try URLEncoding.default.encode(urlRequest, with: param)
            
            // Genres
            case .genreMovies(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            // Series
            case .seriesPopular(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .seriesTopRated(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .seriesOnTV(let parameters):
                return try URLEncoding.default.encode(urlRequest, with: parameters)
            
            case .serie:
                let param = ["api_key": "6e91acae467fcd5fad05efc3e5dae6fd", "language": "en-US",] as [String : Any]
                return try URLEncoding.default.encode(urlRequest, with: param)
            
            default:
                return urlRequest
        }
    }
}
