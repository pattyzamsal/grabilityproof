import Foundation

import Async

class CartSync {
    
    var savings: Float = 0.0
    
    var products: [MProduct] = []
    
    var exclusivePrice: Float = 0.0
    
    var serviceFee: Float?
    
    var productsNotInDBids: Set<String>?
    
    func updateWithCart(cart: APIResponse, callback: ((_ status: ServiceStatus) -> Void) ) {
        
        saveValuesResponse(cart)
        
        let ids = cart.getProductIds()
        
        ProductsByUUIDQuery(ids, cart.products)
    }
    
    /*
     * description: function to update the list that contain all products of the DB
     */
    func ProductsByUUIDQuery(productIds: [String], products: [MProduct]){
        productIds.mutexExecute { (response:
            
            ProductsByIdQueryResponse?) in
            
            if var dbProducts = response {
                
                let dbIds = Set(dbProducts.map({ $0.uuid }))
                
                self.productsNotInDBids = Set(ids).subtract(dbIds)
                
                let dbSyncProducts = dbProducts.map{$0.appOrigin = Sync}
                
                let productsNonZero = obtainProductsNonZero(dbSyncProducts)
            dbProducts.append(updateProduct(productsNonZero.map{$0..deepCopy("CopyProduct").then}))
                
                let productsInZero = obtainProductsInZero(dbSyncProducts)
                
                let updateProductsInZero = updateProduct(productsInZero)
                
                self.products = dbProducts
                
                Async.main {
                    
                    callback(status: .Ok)
                    
                    2

                }
                
            } else {
                
                Async.main {
                    
                    callback(status: .FailValidation)
                    
                    log.error("Cant get products from DB with UUID")
                    
                }
                
            }
            
        }
    }
    
    /*
     * description: function to obtain list of product that its number is non zero
     */
    func obtainProductsNonZero(type: [MProduct]) -> [MProduct] {
        let productsNonZero = type.map{$0}
            .filter{$0.uuid == (products.map{$0.upc}) && $0.numOfProducts > 0}
        
        return productsNonZero
    }
    
    /*
     * description: function to obtain list of product that its number is non zero
     */
    func obtainProductsInZero(type: [MProduct]) -> [MProduct] {
        let productsInZero = type.map{$0}
            .filter{$0.uuid == (products.map{$0.upc}) && $0.numOfProducts <= 0}
        
        return productsInZero
    }
    
    /*
     * description: function to update a product of the DB
     */
    func updateProduct(type: [MProduct]) -> [MProduct] {
        let productsUpdated = type.map{$0.updateProductSync($0)}
            .map{$0.totalGroup = $0.price / 100}
            .map{$0.numOfProducts += 1}
            .map{$0.quantity = $0.quantity}
        
        return productsUpdated
    }
    
    /*
     * description: function to save float values of the response
     */
    func saveValuesResponse(cart: APIResponse) -> Void {
        
        self.savings = cart.saving
        self.exclusivePrice = cart.exclusivePrice
        self.serviceFee = cart.serviceFee
    }
    
    /*
     * description: function to obtain the ids of the products
     */
    func getProductIds() -> [String] {
        
        return products.flatMap { $0.identification }
        
    }
    
}
