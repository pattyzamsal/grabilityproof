//
//  cellSearchCategory.swift
//  ProofGrability
//
//  Created by Patricia Zambrano on 3/20/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit

class cellSearchCategory: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var movie1Label: UILabel!
    @IBOutlet weak var movie2Label: UILabel!
    @IBOutlet weak var movie3Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
