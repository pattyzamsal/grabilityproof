//
//  ViewController.swift
//  ProofGrability
//
//  Description: this view select between Movie or Serie
//
//  Created by Patricia Zambrano on 3/19/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var optionLabel: UILabel!
    
    @IBOutlet weak var movieBtn: UIButton!
    @IBOutlet weak var serieBtn: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let langID = Locale.preferredLanguages[0]
        let lang = (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.languageCode, value: langID)
        
        // Do any additional setup after loading the view, typically from a nib.
        
        optionLabel.text = NSLocalizedString("Choose an option", comment: "")
        movieBtn.setTitle(NSLocalizedString("Movies", comment: ""), for: UIControlState())
        serieBtn.setTitle(NSLocalizedString("Series", comment: ""), for: UIControlState())
        
        movieBtn.addTarget(self, action: #selector(ViewController.movieBtn(_:)), for: .touchUpInside)
        serieBtn.addTarget(self, action: #selector(ViewController.serieBtn(_:)), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
     *  Actions of the buttons in the view
     */
    @IBAction func movieBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "SearchView") as! SearchView
        myVC.searchOption = "Movies"
        navigationController?.pushViewController(myVC, animated: true)
    }

    @IBAction func serieBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "SearchView") as! SearchView
        myVC.searchOption = "Series"
        navigationController?.pushViewController(myVC, animated: true)
    }

}

