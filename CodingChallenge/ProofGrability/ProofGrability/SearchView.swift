//
//  SearchView.swift
//  ProofGrability
//
//  Description: this view select between a search defined or specific search
//
//  Created by Patricia Zambrano on 3/19/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SearchView: UIViewController {

    @IBOutlet weak var searchLabel: UILabel!
    
    @IBOutlet weak var popularBtn: UIButton!
    @IBOutlet weak var topRatedBtn: UIButton!
    @IBOutlet weak var upcomingBtn: UIButton!
    
    @IBOutlet weak var specificBtn: UIButton!
    
    // variable to connect views (ViewController-SearchView)
    var searchOption = ""
    
    // private variables
    var thirdOption = ""
    
    // variables to save objects that are contained in the json of backend
    var listInfo = [JSON]()
    var id = [Int]()
    var originalTitle = [String]()
    var originalLanguage = [String]()
    var popularity = [Int]()
    var releaseDate = [String]()
    
    let param = ["api_key": "6e91acae467fcd5fad05efc3e5dae6fd", "language": "en-US", "page": "1",] as [String : Any]
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchLabel.text = NSLocalizedString("Select a method to the search", comment: "")
        popularBtn.setTitle(NSLocalizedString("Popular " + searchOption, comment: ""), for: UIControlState())
        topRatedBtn.setTitle(NSLocalizedString("Top Rated " + searchOption, comment: "") , for: UIControlState())
        if searchOption == "Movies"{
            upcomingBtn.setTitle(NSLocalizedString("Upcoming " + searchOption, comment: ""), for: UIControlState())
            specificBtn.setTitle(NSLocalizedString("Specific " + searchOption, comment: ""), for: UIControlState())
            specificBtn.isHidden = false
            thirdOption = "Upcoming " + searchOption
        }
        else {
            upcomingBtn.setTitle(NSLocalizedString("On TV " + searchOption, comment: ""), for: UIControlState())
            specificBtn.isHidden = true
            thirdOption = "On TV " + searchOption
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     *  Actions of the buttons in the view
     */
    @IBAction func popularBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "CategoryView") as! CategoryView
        myVC.category = "Popular " + searchOption
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast()
        viewControllers?.append(myVC)
        self.getFromBackend(type: "popular", view: myVC)
    }
    
    @IBAction func topRatedBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "CategoryView") as! CategoryView
        myVC.category = "Top Rates " + searchOption
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast()
        viewControllers?.append(myVC)
        self.getFromBackend(type: "topRated", view: myVC)
    }
    
    @IBAction func upcomingBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "CategoryView") as! CategoryView
        myVC.category = thirdOption
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast()
        viewControllers?.append(myVC)
        self.getFromBackend(type: "upcoming", view: myVC)
    }
    
    @IBAction func specificBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "SpecificSearchView") as! SpecificSearchView
        myVC.searchOption = searchOption
        navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    /*
     *  @param:
     *      type -> string to define if is a movie or serie
     *      view -> pass the view that receive the objects to need set in a table view
     *  description: method to realize the get connection with the backend
     */
    func getFromBackend(type: String, view: CategoryView) {
        if searchOption == "Movies" {
            switch type {
                case "popular":
                    Alamofire.request(Router.moviesPopular(param as [String : AnyObject]))
                        .validate()
                        .responseJSON { response in
                            
                            if response.result.isSuccess{
                                
                                let info = (JSON(response.result.value!))["results"].arrayValue
                                
                                print(info)
                                
                                self.listInfo = info
                                
                                self.SaveData(view: view)
                                
                                SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                                
                            } else {
                                print(response.debugDescription)
                                SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                            }
                    }
                    break
                
                case "topRated":
                    Alamofire.request(Router.moviesTopRated(param as [String : AnyObject]))
                        .validate()
                        .responseJSON { response in
                            
                            if response.result.isSuccess{
                                
                                let info = (JSON(response.result.value!))["results"].arrayValue
                                
                                print(info)
                                
                                self.listInfo = info
                                
                                self.SaveData(view: view)
                                
                                SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                                
                            } else {
                                print(response.debugDescription)
                                SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                            }
                    }
                    break
                
                case "upcoming":
                    Alamofire.request(Router.moviesUpcoming(param as [String : AnyObject]))
                        .validate()
                        .responseJSON { response in
                            
                            if response.result.isSuccess{
                                
                                let info = (JSON(response.result.value!))["results"].arrayValue
                                
                                print(info)
                                
                                self.listInfo = info
                                
                                self.SaveData(view: view)
                                
                                SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                                
                            } else {
                                print(response.debugDescription)
                                SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                            }
                    }
                    break
                
                default:
                    break
            }
        }
        else {
            switch type {
            case "popular":
                Alamofire.request(Router.seriesPopular(param as [String : AnyObject]))
                    .validate()
                    .responseJSON { response in
                        
                        if response.result.isSuccess{
                            
                            let info = (JSON(response.result.value!))["results"].arrayValue
                            
                            print(info)
                            
                            self.listInfo = info
                            
                            self.SaveData(view: view)
                            
                            SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                            
                        } else {
                            print(response.debugDescription)
                            SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                        }
                }
                break
                
            case "topRated":
                Alamofire.request(Router.seriesTopRated(param as [String : AnyObject]))
                    .validate()
                    .responseJSON { response in
                        
                        if response.result.isSuccess{
                            
                            let info = (JSON(response.result.value!))["results"].arrayValue
                            
                            print(info)
                            
                            self.listInfo = info
                            
                            self.SaveData(view: view)
                            
                            SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                            
                        } else {
                            print(response.debugDescription)
                            SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                        }
                }
                break
                
            case "upcoming":
                Alamofire.request(Router.seriesOnTV(param as [String : AnyObject]))
                    .validate()
                    .responseJSON { response in
                        
                        if response.result.isSuccess{
                            
                            let info = (JSON(response.result.value!))["results"].arrayValue
                            
                            print(info)
                            
                            self.listInfo = info
                            
                            self.SaveData(view: view)
                            
                            SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                            
                        } else {
                            print(response.debugDescription)
                            SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                        }
                }
                break
                
            default:
                break
            }
        }
    }
    
    /*
     *  description: function to save the data of backend
     */
    func SaveData(view: CategoryView){
        var j = 0
        for i in 0..<self.listInfo.count {
            self.id.insert(self.listInfo[i]["id"].int!, at: j)
            if searchOption == "Movies"{
                self.originalTitle.insert(self.listInfo[i]["original_title"].stringValue, at: j)
                self.releaseDate.insert(self.listInfo[i]["release_date"].stringValue.components(separatedBy: "-")[0], at: j)
            }
            else {
                self.originalTitle.insert(self.listInfo[i]["original_name"].stringValue, at: j)
                self.releaseDate.insert(self.listInfo[i]["first_air_date"].stringValue.components(separatedBy: "-")[0], at: j)
            }
            self.originalLanguage.insert(self.listInfo[i]["original_language"].stringValue, at: j)
            self.popularity.insert(self.listInfo[i]["popularity"].int!, at: j)
            j += 1
        }
        
        SomeManager.sharedInstance.id = self.id
        SomeManager.sharedInstance.originalTitle = self.originalTitle
        SomeManager.sharedInstance.popularity = self.popularity
        SomeManager.sharedInstance.originalLanguage = self.originalLanguage
        SomeManager.sharedInstance.releaseDate = self.releaseDate
        
        view.objects = SomeManager.sharedInstance
        navigationController?.pushViewController(view, animated: true)
    }

}
