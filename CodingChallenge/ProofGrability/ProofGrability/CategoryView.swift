//
//  CategoryView.swift
//  ProofGrability
//
//  Description: this view show a search defined and some details of object (movie or serie)
//
//  Created by Patricia Zambrano on 3/19/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit
import SVProgressHUD

class CategoryView: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var categoryTable: UITableView!
    
    var category = ""
    var objects = SomeManager()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        categoryLabel.text = NSLocalizedString(category, comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (SVProgressHUD.isVisible()) {
            SVProgressHUD.dismiss()
        }
        self.categoryTable.reloadData()
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    //****************
    //
    //  TableView
    //
    //***************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SomeManager.sharedInstance.id.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.categoryTable.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! cellCategoryTableView
        
        if indexPath.row < objects.id.count {
            cell.nameLabel.text = "Title: " + objects.originalTitle[indexPath.row]
            cell.nameLabel.numberOfLines = 0
            cell.nameLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.languageLabel.text = "Language: " + objects.originalLanguage[indexPath.row]
            cell.popularityLabel.text = "Popularity: " + "\(objects.popularity[indexPath.row])"
            cell.releaseDateLabel.text = "Year release: " + objects.releaseDate[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

}
