//
//  cellCategoryTableView.swift
//  ProofGrability
//
//  Description: this class contains the cell's elements of the table view at CategoryView
//
//  Created by Patricia Zambrano on 3/19/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit

class cellCategoryTableView: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
