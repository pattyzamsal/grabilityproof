//
//  SearchCategoryView.swift
//  ProofGrability
//
//  Description: this view show some categories and some details of object (movie or serie)
//
//  Created by Patricia Zambrano on 3/20/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SearchCategoryView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var categoriesTableView: UITableView!
    
    // variable to connect views (SearchView-SearchCategoryView)
    var searchName = ""

    // variables to save objects that are contained in the json of backend
    var listInfo = [JSON]()
    var listCategories = [MoviesAtGenres]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        listCategories = [MoviesAtGenres]()
        obtainFromBackend()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     *  description: function to obtain the search of the movie
     */
    func obtainFromBackend () {
        let param = ["api_key": "6e91acae467fcd5fad05efc3e5dae6fd",
                     "query": searchName,] as [String : Any]
        Alamofire.request(Router.movieSearch(param as [String : AnyObject]))
            .validate()
            .responseJSON { response in
                
                if response.result.isSuccess{
                    
                    let info = (JSON(response.result.value!))["results"].arrayValue
                    
                    print(info)
                    self.listInfo = info
                    
                    for i in 0..<self.listInfo.count {
                        var genres = self.listInfo[i]["genre_ids"].arrayValue
                        let title = self.listInfo[i]["original_title"].stringValue
                        if (genres.count != 0) {
                            if (self.listCategories.isEmpty) {
                                for j in 0..<genres.count {
                                    let newMovieAtGenre = MoviesAtGenres()
                                    newMovieAtGenre.id = genres[j].intValue
                                    newMovieAtGenre.nameMovie.append(title)
                                    self.listCategories.append(newMovieAtGenre)
                                }
                            }
                            else {
                                // case when the id was previously saved
                                for j in 0..<self.listCategories.count {
                                    if (genres.contains(JSON(self.listCategories[j].id))) {
                                        self.listCategories[j].nameMovie.append(title)
                                        genres.remove(at: genres.index(of: JSON(self.listCategories[j].id))!)
                                    }
                                }
                                // case the id don't exists at list of categories
                                if genres.count != 0 {
                                    for j in 0..<genres.count {
                                        let newMovieAtGenre = MoviesAtGenres()
                                        newMovieAtGenre.id = genres[j].intValue
                                        newMovieAtGenre.nameMovie.append(title)
                                        self.listCategories.append(newMovieAtGenre)
                                    }
                                }
                            }
                        }
                    }
                    
                    SVProgressHUD.showSuccess(withStatus: NSLocalizedString("Successfull search", comment: ""))
                    self.categoriesTableView.reloadData()
                    
                } else {
                    print(response.debugDescription)
                    SVProgressHUD.showError(withStatus: NSLocalizedString("Error while trying to search the artist", comment: ""))
                }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

    //****************
    //
    //  TableView
    //
    //***************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! cellSearchCategory
        
        cell.tag = indexPath.row
        if (indexPath.row < self.listCategories.count) {
            cell.categoryLabel.text = Genres.sharedInstance.obtainGenreSpecific(id: self.listCategories[indexPath.row].id)
            if (self.listCategories[indexPath.row].nameMovie.count >= 3) {
                cell.movie1Label.text = self.listCategories[indexPath.row].nameMovie[0]
                cell.movie2Label.text = self.listCategories[indexPath.row].nameMovie[1]
                cell.movie3Label.text = self.listCategories[indexPath.row].nameMovie[2]
                cell.movie2Label.isHidden = false
                cell.movie3Label.isHidden = false
            }
            else if (self.listCategories[indexPath.row].nameMovie.count == 2) {
                cell.movie1Label.text = self.listCategories[indexPath.row].nameMovie[0]
                cell.movie2Label.text = self.listCategories[indexPath.row].nameMovie[1]
                cell.movie2Label.isHidden = false
                cell.movie3Label.isHidden = true
            }
            else {
                cell.movie1Label.text = self.listCategories[indexPath.row].nameMovie[0]
                cell.movie2Label.isHidden = true
                cell.movie3Label.isHidden = true
            }
        }
        return cell
    }
}
