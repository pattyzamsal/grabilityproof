//
//  SpecificSearchView.swift
//  ProofGrability
//
//  Description: this view allow to do a search with name of object (movie or serie)
//
//  Created by Patricia Zambrano on 3/19/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SpecificSearchView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    
    // variable to connect views (SearchView-SpecificSearchView)
    var searchOption = ""
    
    // variables to save objects that are contained in the json of backend
    var listInfo = [JSON]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(SpecificSearchView.handleSingleTap(_:)))
        self.view.addGestureRecognizer(singleFingerTap)
        
        // Activate delegate of the text fields
        self.searchField.delegate = self
        
        if (Genres.sharedInstance.genresIsEmpty()) {
            getGenresOfBackend()
        }
        
        searchField.placeholder = NSLocalizedString("Search a specific " + searchOption, comment: "")
        SearchButton.setTitle(NSLocalizedString("Search", comment: ""), for: UIControlState())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     *  description: function to obtain the genres of API
     */
    func getGenresOfBackend () {
        let param = ["api_key": "6e91acae467fcd5fad05efc3e5dae6fd",
                    ] as [String : Any]
        Alamofire.request(Router.genreMovies(param as [String : AnyObject]))
            .validate()
            .responseJSON { response in
                
                if response.result.isSuccess{
                    
                    let info = (JSON(response.result.value!))["genres"].arrayValue
                    
                    print(info)
                    
                    self.listInfo = info
                    
                    self.saveGenres()
                }
        }
    }
    
    /*
     *  description: function to obtain all genres of the movies
     */
    func saveGenres() {
        for i in 0..<self.listInfo.count {
            let newId = self.listInfo[i]["id"].intValue
            let newName = self.listInfo[i]["name"].stringValue
            Genres.sharedInstance.id.insert(newId, at: i)
            Genres.sharedInstance.name.insert(newName, at: i)
        }
    }
    
    /*
     *  Actions of the buttons in the view
     */
    @IBAction func searchButton(_ sender: UIButton) {
        self.view.endEditing(true)
        let myVC = storyboard?.instantiateViewController(withIdentifier: "SearchCategoryView") as! SearchCategoryView
        myVC.searchName = searchField.text!
        navigationController?.pushViewController(myVC, animated: true)
    }

    /*
     *  description: to define when the user realize a tap at the screen
     */
    func handleSingleTap(_ recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    //****************
    //
    //  TextField delegate
    //
    //***************
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
