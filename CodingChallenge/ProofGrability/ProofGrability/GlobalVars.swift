//
//  GlobalVars.swift
//  ProofGrability
//
//  Description: this class is to save objects obtained from backend
//
//  Created by Devstn4 on 3/20/17.
//  Copyright © 2017 Patricia Zambrano. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

/*
 *  description: Class to save a list of movies or series
 */
class SomeManager {
    
    var id = [Int]()
    var originalTitle = [String]()
    var originalLanguage = [String]()
    var popularity = [Int]()
    var releaseDate = [String]()
    
    static let sharedInstance = SomeManager()
}

/*
 *  description: Class to save the all genres of movies
 */
class Genres {
    
    var id = [Int]()
    var name = [String]()
    
    static let sharedInstance = Genres()
    
    /*
     *  description: function to know if the genres has been obtained from the API
     */
    func genresIsEmpty () -> Bool {
        if (id.isEmpty && name.isEmpty) {
            return true
        }
        else {
            return false
        }
    }
    
    /*
     *  description: function to obtain the name of the genre
     */
    func obtainGenreSpecific (id: Int) -> String {
        var nameGenre = ""
        for i in 0..<Genres.sharedInstance.id.count {
            if (id == Genres.sharedInstance.id[i]) {
                nameGenre = Genres.sharedInstance.name[i]
                break
            }
        }
        return nameGenre
    }
}

/*
 *  description: Class to save the movies of a specific genre
 */
class MoviesAtGenres {
    var id = 0
    var nameMovie = [String]()
}
